import gevent; from gevent import monkey; gevent.monkey.patch_all()
import requests
from stock_analyzer import analyze
import services
#Base, engine, db = services.get_sqlalchemy()
#import models
import csv
import random
from lib import ystockquote


def update_symbol(symbol, analysis_file, write_headers=False):
    try:
        data = ystockquote.get_ticker(symbol, ['average daily volume'] )
    except:
        print "Error getting data for %s. Skipping" % symbol
        return
    try:
        volume = int(data.get(symbol, {}).get('average daily volume'))
    except:
        volume = 0
    if volume < 50000:
        print "Not analyzing %s. Volume is %s" % (symbol, volume)
        return

    analysis = analyze(symbol)
    if analysis:
        write_headers = analysis_file.tell() == 0
        csv_data =  analysis.to_csv(header=write_headers)
        analysis_file.write( csv_data )

    print "Finished %s" % symbol



def update_symbols():
    analysis_file = open('analysis.csv', 'w')
    markets = ['nasdaq', 'nyse', 'amse']
    columns = False

    for m in markets:
        csv_file = 'conf/%s.csv' % m
        print csv_file
        with open(csv_file, 'rb') as csvfile:
            reader = csv.reader(csvfile, delimiter=',')
            next(reader, None)
            symbols = []
            for row in reader:
                symbol = row[0]
                company_name = row[1]
                sector = row[5]
                industry = row[6]
                symbols.append(symbol)

            random.shuffle(symbols)

        import gevent
        from gevent.pool import Pool
        pool = Pool(10)

        from functools import partial
        u = partial(update_symbol, analysis_file=analysis_file)

        pool.map(u, symbols)
        pool.join()
        print "Done with all symbols"


def top_picks():
    import pandas as pd
    import numpy as np
    from datetime import datetime, timedelta
    from dateutil import parser

    pd.set_option('display.max_rows', 500)
    data = pd.read_csv('analysis.csv', parse_dates=['date'])
    data['date'] = data['date'].apply(lambda x: x.strftime('%Y-%m-%d'))

    symbols = np.unique( data[['symbol']] )
    print "Symbols analyzed: %s" % len(symbols)
    #data = data[data.pattern.str.contains("(?i)baby")]
    last_date = ( np.unique(data['date']) ) [-1]
    last_date = parser.parse(last_date)
    #data = data.query('date == "%s"' % last_date)
    r = range(1,7)
    q = ' or '.join( ['day_%s_percentage_up > 80 ' % x for x in r] )
    data = data.query(q)

    columns = ['date', 'symbol', 'pattern', 'day_1_total']
    [ columns.extend( [ 'day_%s_percentage_up' % x  ] ) for x in r]
    #print columns
    data = data.ix[:,columns].sort(columns=['date', 'day_1_total','day_1_percentage_up'], ascending=[True, False, False] )
    data = data[['date', 'symbol']].sort(columns=['date']).drop_duplicates(['date','symbol'])
    data = data.groupby('date')['symbol'].apply(', '.join)
    return data




if __name__ == '__main__':

    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-a', '--analyze', action='store_true')
    args = parser.parse_args()

    if args.analyze:
        update_symbols()

    print top_picks()


