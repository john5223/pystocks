from gevent import monkey; monkey.patch_all()
import sys
sys.path.insert(0,'./web')
import ujson as json

import logging
logging.basicConfig(level='DEBUG')
logging.getLogger("requests").setLevel(logging.INFO)

import bottle
import json, bson
import decimal
import datetime

app = bottle.app()

from pandas import DataFrame

def json_default(obj):
    if isinstance(obj, DataFrame):
        temp = obj.copy().reset_index()
        if 'Date' in temp.columns:
            temp['Date'] = temp['Date'].apply(lambda x: x.strftime('%Y-%m-%d'))
        ret =  temp.to_dict(orient='records')
        return ret
    if isinstance(obj, decimal.Decimal):
        return float(obj)
    if isinstance(obj, set):
        return list(obj)
    if isinstance(obj, xrange):
        return [x for x in obj]
    try:
        bson.json_util.default(obj)
    except Exception as e:
        print e
        print "Can't decode %s, skipping" % obj
        return None

app.install(bottle.JSONPlugin(json_dumps=lambda body: json.dumps(body, default=json_default)))


from web.controllers import main_controller
from web.controllers import static_controller
from web.controllers import errors_controller


if __name__ == '__main__':
    bottle.run(app=app, port=8085, server='gevent', debug=True)
