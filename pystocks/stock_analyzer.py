import gevent;
from gevent import monkey; gevent.monkey.patch_all()
from gevent import monkey; monkey.patch_socket()

from datetime import datetime

import numpy as np
import pandas as pd
from pandas.io.data import DataReader
pd.set_option('expand_frame_repr', False)

import yaml
stream = open("conf/candle_patterns.yaml", 'r')
candle_patterns = yaml.load(stream)

all_candle_variables = set()

for pattern in candle_patterns:
    candle_patterns[pattern]['name'] = pattern
    candle_patterns[pattern]['key'] = pattern.lower().replace(' ','_')

    VALID_VARIABLES = ['h','l','c','o','v']
    chars_to_space = ['(',')','and', 'or','<','>','-','+','*','/','=', '  ']
    formula = candle_patterns[pattern].get('formula').lower()
    for c in chars_to_space:
       formula = formula.replace(c, ' %s ' % c)
    formula = formula.replace('  ', ' ')
    formula = formula.replace('  ', ' ')
    for c in ['<','>','=']:
       formula = formula.replace('%s =' % c, '%s=' % c)
    formula = formula.replace(' = ',' == ')
    candle_patterns[pattern]['formula'] = formula
    if not candle_patterns[pattern].get('variables'):
        for c in chars_to_space:
            formula = formula.replace(c, ' ')
        ta_variables = [x for x in formula.split(' ') if x and x[0] in VALID_VARIABLES]
        ta_variables = list(set(ta_variables))
        candle_patterns[pattern]['variables'] = ta_variables
        all_candle_variables.update(ta_variables)

    days = candle_patterns[pattern].get('days')
    if not days:
        days = 1
        for v in candle_patterns[pattern]['variables']:
            num = int(v[1:] or 0)+1
            if num > days:
                days = num
        candle_patterns[pattern]['days'] = days


#RSI = 100 - 100/(1 + RS*)
#*Where RS = Average of x days' up closes / Average of x days' down closes.

class NotAnalyzing(Exception):
    pass


class Analyzer():

    def __init__(self, symbol, **kwargs):
        self.symbol_map = {'O': 'Open', 'C':'Close','H':'High','L':'Low','V':'Volume'}
        self.candle_patterns = candle_patterns
        self.RECENT_PATTERN_DAYS = 15
        self.ANALYZE_RANGE = range(1,13)
        self.start_date = '2002-01-1'
        self.symbol = symbol
        self.historical_accuracy = None


    def _get_price_history(self):
        try:
            hist = DataReader(self.symbol,  "yahoo", self.start_date, datetime.now().strftime('%Y-%m-%d'))
        except IOError as e:
            raise NotAnalyzing("No price history for %s" % self.symbol)
        if hist.empty:
            raise NotAnalyzing("No price history for %s" % self.symbol)
        elif len(hist) < 50:
            raise NotAnalyzing("Not analyzing %s with only %s days of trading" % (self.symbol,len(hist)))
        self.history = hist

        self.history_json = hist.copy().reset_index()
        self.history_json['Date'] = self.history_json['Date'].apply(lambda x: x.strftime('%Y-%m-%d'))
        self.history_json =self.history_json.to_dict(orient='records')
        self.dates = hist.index.values
        return self

    def _analyze_candle_patterns(self):
        hist = self.history.copy()
        hist.columns = ['o', 'h', 'l', 'c', 'v', 'adj_close']
        #get variable values (o1, c1, h2, etc.)
        for var in all_candle_variables:
            if var not in hist.columns:
                col = var[0]
                i = int(var[1:])
                hist[var] = hist[col].shift(i)

        #generate future prices data
        for i in self.ANALYZE_RANGE:
            hist['f_%s_up' % i] = hist['c'].shift(-1*i) > hist['c']
            hist['future_perc_increase_%s' % i] = ( hist['c'].shift(-1*i) - hist['c'] ) / hist['c']
            hist['past_perc_increase_%s' % i] = ( hist['c'] - hist['c'].shift(i) ) / hist['c'].shift(i)
            query_str = ('( future_perc_increase_{i} > 0 > past_perc_increase_{i} ) or (  future_perc_increase_{i} < 0 < past_perc_increase_{i} ) ').format(i=i)
            hist['f_%s_reversal' % i] = hist.index.isin( hist.query(query_str).index.values )

        #Check true or false values for candle patterns
        for pattern_name, pattern in candle_patterns.iteritems():
            formula = pattern.get('formula')
            pattern_key = pattern.get('key')
            hist[pattern_key] = hist.index.isin( hist.query(formula).index.values )

        #generate list of patterns for each day
        def patterns(series):
            return ', '.join( [v.get('name') for k,v in candle_patterns.iteritems() if series[v.get('key')] == True] )
        hist['patterns'] = hist.apply(patterns,axis=1)
        self.candle_history = hist

        pattern_accuracies = {}
        for pattern_name, pattern in candle_patterns.iteritems():
            pattern_key = pattern.get('key')
            pattern_accuracies[pattern_key] = { 'symbol': self.symbol, 'pattern': pattern_name }

            max_days = max(self.ANALYZE_RANGE)
            available_prediction_history = hist[max_days:(-1*max_days)]
            for i in self.ANALYZE_RANGE:
                total = len( hist.query("{pattern} == True".format(pattern=pattern_key)) )
                num_up = len ( hist.query("{pattern} == True and f_{i}_up == True".format(i=i, pattern=pattern_key)) )
                num_reversal = len ( hist.query('{pattern} == True and f_{i}_reversal == True'.format(i=i, pattern=pattern_key)) )
                accuracy_data = { 'day_%s_num_up'%i: num_up,
                                  'day_%s_num_reversal'%i: num_reversal,
                                  'day_%s_total'%i: total }
                pattern_accuracies[pattern_key].update( accuracy_data )

        pa = pd.DataFrame( [v for k,v in pattern_accuracies.iteritems() ])
        pa = pa.set_index(['symbol','pattern'])
        #calculate percentages and reorder columns
        cols = []
        for i in self.ANALYZE_RANGE:
            #calculate percentages
            pa['day_%s_percentage_up' % i] = (( pa['day_%s_num_up'%i] / pa['day_%s_total'%i].replace({0: np.nan}) ) * 100).round(2)
            cols.append('day_%s_percentage_up' % i)
            pa['day_%s_percentage_reversal' % i] = (( pa['day_%s_num_reversal'%i] / pa['day_%s_total'%i].replace({0: np.nan}) ) * 100).round(2)
            cols.append('day_%s_percentage_reversal' % i)

        for i in self.ANALYZE_RANGE:
            cols.append('day_%s_total'%i)
            cols.append('day_%s_num_up'%i)
            cols.append('day_%s_num_reversal'%i)

        pa = pa[ cols ] # reorder columns
        self.historical_accuracy = pa
        #import pdb; pdb.set_trace()
        #print hist
        return self


    def analyze_candle_patterns(self):
        self._get_price_history()
        self._analyze_candle_patterns()

    def to_csv(self, header=True):
        recent_hist = self.candle_history [-12:] [['patterns']]
        recent_hist['patterns'] = recent_hist.patterns.str.split(', ').tolist()
        recent_hist = pd.concat([pd.Series(row.Date, row.patterns)  for _, row in recent_hist.reset_index().iterrows()]).reset_index()
        recent_hist.columns = ['pattern', 'date']
        ha = self.historical_accuracy.reset_index().set_index('pattern')
        recent_hist = recent_hist.set_index(['pattern'])
        recent_hist = recent_hist.join(ha).reset_index().sort(['symbol', 'date', 'pattern']).set_index(['date', 'symbol', 'pattern'])
        return recent_hist.to_csv(header=header, date_format='%Y%m%d')



def analyze(symbol):
    try:
        analyzer = Analyzer(symbol)
        analyzer.analyze_candle_patterns()
        return analyzer
    except NotAnalyzing as e:
        import logging, traceback
        tb = traceback.format_exc()
        print "Error: %s: %s" % (symbol, e)
    except Exception as e:
        import logging, traceback
        tb = traceback.format_exc()
        print tb
        print "Error: %s" % symbol


if __name__ == '__main__':
    symbol = 'yhoo'

    a = analyze(symbol)
    print a.to_csv()
    '''
    analyzer = Analyzer(symbol)
    analyzer.analyze_candle_patterns()

    #print analyzer.history
    print analyzer.candle_history[ ['patterns'] ]
    print analyzer.historical_accuracy[ ['day_%s_percentage_up'%i for i in analyzer.ANALYZE_RANGE] ]
    print analyzer.historical_accuracy[ ['day_%s_percentage_reversal'%i for i in analyzer.ANALYZE_RANGE] ]
    '''

