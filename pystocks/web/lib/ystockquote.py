import logging
logger = logging.getLogger('ystockquote')

import requests

base_uri = "http://finance.yahoo.com/d/quotes.csv"

STAT_LOOKUP = {
"a": "Ask",
"y": "Dividend Yield",
"b": "Bid",
"d": "Dividend per Share",
"b2": "Ask (Realtime)",
"r1": "Dividend Pay Date",
"b3": "Bid (Realtime)",
"q": "Ex-Dividend Date",
"p": "Previous Close",
"o": "Open",
"c1": "Change",
"d1": "Last Trade Date",
"c": "Change & Percent Change",
"d2": "Trade Date",
"c6": "Change (Realtime)",
"t1": "Last Trade Time",
"k2": "Change Percent (Realtime)",
"p2": "Change in Percent",
"c8": "After Hours Change (Realtime)",
"m5": "Change From 200 Day Moving Average",
"c3": "Commission",
"m6": "Percent Change From 200 Day Moving Average",
"g": "Day's Low",
"m7": "Change From 50 Day Moving Average",
"h": "Day's High",
"m8": "Percent Change From 50 Day Moving Average",
"k1": "Last Trade (Realtime) With Time ",
"m3": "50 Day Moving Average",
"l": "Last Trade (With Time)",
"m4": "200 Day Moving Average",
"l1": "Last Trade (Price Only) ",
"t8": "1 yr Target Price",
"w1": "Day's Value Change",
"g1": "Holdings Gain Percent",
"w4": "Day's Value Change (Realtime)",
"g3": "Annualized Gain",
"p1": "Price Paid",
"g4": "Holdings Gain",
"m": "Day's Range",
"g5": "Holdings Gain Percent (Realtime)",
"m2": "Day's Range (Realtime)",
"g6": "Holdings Gain (Realtime)",
"52": "Week Pricing Symbol Info",
"k": "52 Week High",
"v": "More Info",
"j": "52 week Low",
"j1": "Market Capitalization",
"j5": "Change From 52 Week Low ",
"j3": "Market Cap (Realtime)",
"k4": "Change From 52 week High",
"f6": "Float Shares",
"j6": "Percent Change From 52 week Low ",
"n": "Name",
"k5": "Percent Change From 52 week High",
"n4": "Notes",
"w": "52 week Range",
"s": "Symbol",
"s1": "Shares Owned",
"x": "Stock Exchange",
"j2": "Shares Outstanding",
"v": "Volume",
"a5": "Ask Size",
"b6": "Bid Size",
"k3": "Last Trade Size ",
"t7": "Ticker Trend",
"a2": "Average Daily Volume",
"t6": "Trade Links",
"i5": "Order Book (Realtime)",
"l2": "High Limit",
"e": "Earnings per Share",
"l3": "Low Limit",
"e7": "EPS Estimate Current Year",
"v1": "Holdings Value",
"e8": "EPS Estimate Next Year",
"v7": "Holdings Value (Realtime)",
"e9": "EPS Estimate Next Quarter",
"s6": "Revenue",
"b4": "Book Value",
"j4": "EBITDA",
"p5": "Price / Sales",
"p6": "Price / Book",
"r": "P/E Ratio",
"r2": "P/E Ratio (Realtime)",
"r5": "PEG Ratio",
"r6": "Price / EPS Estimate Current Year",
"r7": "Price / EPS Estimate Next Year",
"s7": "Short Ratio "
}

STAT_LOOKUP = {k.lower(): v.lower() for k,v in STAT_LOOKUP.iteritems() }
REVERSE_STAT_LOOKUP = {v.lower() : k.lower() for k,v in STAT_LOOKUP.iteritems() }

def get_ticker(ticker, stat, format='json'):
    if isinstance(stat, str):
        stat = stat.split(',')
    f = []
    for s in stat:
        s = s.lower()
        if s in STAT_LOOKUP.keys():
            f.append(s)
        elif s in REVERSE_STAT_LOOKUP.keys():
            f.append( REVERSE_STAT_LOOKUP.get(s) )
        else:
            logger.warning("Skipping %s" % s)
    if not f: f = ['b2']
    url = base_uri + "?s=%s&f=%s" % (ticker, ''.join(f) )
    resp = requests.get(url)
    content = resp.text.decode().strip()
    columns = [STAT_LOOKUP.get(x) for x in f]
    if format == 'csv':
        return ','.join(columns) + '\n' + content
    elif format == 'json':
        ret = {}
        symbols = ticker.split(',')
        content = content.split('\r\n')
        for i, data in enumerate( content ) :
            data = data.split(',')
            symbol = symbols[i]
            if all( x == 'N/A' for x in data ):
                ret[symbol] = None
            else:
                ret[symbol] = {}
                for j, col in enumerate(columns):
                    ret[symbol][col] = data[j]
        return ret


from urllib import urlencode

def get_historical_prices(symbol, start_date, end_date):
    """
    Get historical prices for the given ticker symbol.
    Date format is 'YYYY-MM-DD'

    Returns a nested dictionary (dict of dicts).
    outer dict keys are dates ('YYYY-MM-DD')
    """
    params = urlencode({
        's': symbol,
        'a': int(start_date[5:7]) - 1,
        'b': int(start_date[8:10]),
        'c': int(start_date[0:4]),
        'd': int(end_date[5:7]) - 1,
        'e': int(end_date[8:10]),
        'f': int(end_date[0:4]),
        'g': 'd',
        'ignore': '.csv',
    })
    url = 'http://ichart.yahoo.com/table.csv?%s' % params
    resp = requests.get(url)
    content = str(resp.text.decode('utf-8').strip())
    daily_data = content.splitlines()
    hist_dict = dict()
    keys = daily_data[0].split(',')
    for day in daily_data[1:]:
        day_data = day.split(',')
        date = day_data[0]
        if len(day_data) < 5:
            return None
        hist_dict[date] = \
            {keys[1]: day_data[1],
             keys[2]: day_data[2],
             keys[3]: day_data[3],
             keys[4]: day_data[4],
             keys[5]: day_data[5],
             keys[6]: day_data[6]}
    return hist_dict
