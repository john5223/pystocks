import logging
logger = logging.getLogger(__name__)

import sqlalchemy

_service_cache = {}

def get_config():
    if not _service_cache.get('config'):
        _service_cache['config'] = {}
    return _service_cache.get('config')

def get_sqlalchemy_engine():
    engine = _service_cache.get('sqlalchemy',{}).get('engine')
    if engine:
        return engine

    config = get_config()
    db = config.get('postgres',{}).get('db')
    #write_user = config.get('postgres',{}).get('write_user')
    #write_password = config.get('postgres',{}).get('write_password')
    host = config.get('postgres',{}).get('host')
    if host:
        host = host if '5432' in host else host + ':5432'
        postgres_write_uri = "postgres://%s/%s" % (host, db)
        engine = sqlalchemy.create_engine(postgres_write_uri, pool_recycle=3600, echo=False)
        config['sqlalchemy'] = config.get('sqlalchemy', {})
        config['sqlalchemy']['engine'] = engine
    else:
        engine = sqlalchemy.create_engine('sqlite:///analysis.db')
    return engine


def get_sqlalchemy(postgres_uri=None, create_and_upgrade=True):
    config = get_config()
    _service_cache['sqlalchemy'] = _service_cache.get('sqlalchemy', {})

    alchemy = _service_cache.get('sqlalchemy')
    Base = alchemy.get('Base')
    engine = alchemy.get('engine')
    db = _service_cache.get('sqlalchemy').get('db')
    if not (Base and engine and db):
        #from sqlalchemy.ext.declarative import declarative_base, DeferredReflection
        from models import Base
        engine = get_sqlalchemy_engine()
        db = sqlalchemy.orm.sessionmaker()(bind=engine)
        config['sqlalchemy'] = {'Base': Base, 'engine': engine, 'db': db}
        #Models (dependent on sql alchemy Base)
        import models
        Base.metadata.create_all(engine)
        if create_and_upgrade:
            logger.info("Checking tables. Please wait. ")
            from services.sqlalchemy_service import create_and_upgrade_tables
            create_and_upgrade_tables(engine, Base.metadata)
    return Base, engine, db

