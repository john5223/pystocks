import logging
logger = logging.getLogger(__name__)

from bottle import get, template

import json
from web.lib import ystockquote
from stock_analyzer import Analyzer

import numpy as np

@get('/api/symbol/<symbol>')
def get_symbol(symbol):
    symbol = symbol.upper()
    columns  = ['ask', 'ask (realtime)', 'bid', 'bid (realtime)',
                '52 week high', '52 week low',
                'volume',
                'revenue', 'dividend yield',
                '50 Day Moving Average',
                '200 Day Moving Average',
                'more info', 'EBITDA', 'p/e ratio' ]
    data = ystockquote.get_ticker(symbol, columns)
    return data


@get('/api/patterns/<symbol>')
def analyze_symbol(symbol):
    analyzer = Analyzer(symbol)
    analyzer.analyze_candle_patterns()
    #logger.info(history)

    #candle history
    columns = ['patterns'] + ['%s'%x for x in analyzer.ANALYZE_RANGE]
    days = max(analyzer.ANALYZE_RANGE)
    candle_history = analyzer.candle_history [ (-1)*days : ] [ ['patterns', 'c' ] ]
    candle_history.columns = ['patterns', 'close']

    #pattern accuracies
    ha = analyzer.historical_accuracy.reset_index().drop('symbol',1).set_index(['pattern'])
    index_to_json = lambda ha: {x: json.loads(ha.ix[x].to_json()) for x in ha.index}
    historical_accuracy = index_to_json(ha)

    ret = {'symbol': symbol,
           'ANALYZE_RANGE': analyzer.ANALYZE_RANGE,
           'history': analyzer.history_json,
           'candle_history': candle_history,
           'historical_accuracy': historical_accuracy,
           'candle_patterns': analyzer.candle_patterns
            }
    return ret



from worker import top_picks

@get('/')
def home():
    top_symbols = top_picks()
    #top_symbols['date'] = top_symbols ['date'].apply(lambda x: x.strftime('%Y-%m-%d'))
    data = { 'top_picks': top_symbols }
    return template('home.html', data)
