from bottle import route, static_file

@route('/:path#.+#', name='static')
@route('/static/:path#.+#', name='static')
def static(path):
    return static_file(path, root='static')
