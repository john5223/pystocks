
var app = angular.module('StocksApp',['ui.bootstrap'],function($interpolateProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
});

app.filter('reverse', function() {
  return function(items) {
    return items.slice().reverse();
  };
});


app.directive('popover', function(){
    return {
        restrict: 'A',
        link: function($scope, element, attrs){
            element.hover(function(){
                // on mouseenter
                popover_id = element.attr('id').split(' ').join('').split('-').join('') + "_popover" ;
                title = $('#' + popover_id).find( ".popover-title" ).text();
                content = $('#' + popover_id).find( ".popover-content" ).html();
                attrs.$set('originalTitle', title );
                attrs.$set('content', content );
                placement =  "right";
                element.popover({html: true,
                                container: 'body',
                                animation: true,
                                placement: function (context, source) {
                                                        var position = $(source).position();
                                                        console.log(position);
                                                        if (position.left > 800) {
                                                            return "left";
                                                        }
                                                        if (position.left < 220) {
                                                            return "right";
                                                        }
                                                        if (position.top < 110){
                                                            return "top";
                                                        }
                                                        return "bottom";
                                                    },
                                template: '<div class="popover popover-medium"><div class="arrow"></div><div class="popover-inner"><h3 class="popover-title"></h3><div class="popover-content"><p></p></div></div></div>',
                              });
                element.popover('show');
            }, function(){
                // on mouseleave
                //element.popover('hide');
            });
        }
    };
});


app.controller("SymbolController", function($scope, $http, $modal, $log) {

    $scope.portfolio = {};
    $scope.pattern_data = {};
    $scope.collapsed = {};

    $scope.interesting_high = 70;
    $scope.interesting_low = 30;

    $scope.accuracy_badges = function(accuracy) {
        ret = {'green': 0, 'yellow': 0, 'red': 0};
        for ( i in _.range(1, 12) ) {
            total = accuracy[ 'day_' + i + '_total' ];
            percentage_reversal = accuracy[ 'day_' + i + '_percentage_reversal' ] ;
            percentage_up = accuracy[ 'day_' + i + '_percentage_up' ] ;
            if (total >= 5) {
                if (percentage_reversal > $scope.interesting_high) {
                    ret['green'] += 1;
                } else if (percentage_reversal < $scope.interesting_low ) {
                    ret['red'] += 1;
                }
                if (percentage_up > $scope.interesting_high) {
                    ret['green'] += 1;
                } else if (percentage_up < $scope.interesting_low ) {
                    ret['red'] += 1;
                }

            } else if (total > 1) {
                if (percentage_reversal > $scope.interesting_high) {
                    ret['yellow'] += 1;
                } else if (percentage_reversal > $scope.interesting_low ) {
                    ret['yellow'] += 1;
                }
                if (percentage_up > $scope.interesting_high) {
                    ret['yellow'] += 1;
                } else if (percentage_up < $scope.interesting_low ) {
                    ret['yellow'] += 1;
                }
            }
        }
        return ret;
    }

    $scope.alertClass= function(a, percentage_field, total_field){
        value = a[percentage_field]  ;
        total = a[total_field] ;
        if (total >= 5) {
            if (value > $scope.interesting_high) {
                return "alert-success";
            } else if (value < $scope.interesting_low) {
                return "alert-danger";
            } else{
                return ""
            }
        } else if (total > 1) {
            if (value > $scope.interesting_high) {
                return "alert-warning";
            } else if (value < $scope.interesting_low ) {
                return "alert-warning";
            } else{
                return ""
            }
        }


    }
    $scope.remove_symbol = function(symbol) {
        console.log("Removing " + symbol);
        $('#' + symbol).hide();
        delete pattern_data[symbol];
    };


    function load_patterns(symbol) {
        $http({
                    method  : 'GET',
                    url     : 'api/patterns/' + symbol,
                })
                .success(function(data) {
                    $('.' + symbol + '_loader').hide();
                    $scope.pattern_data[symbol] = data ;
                    recent_history = [];
                    angular.forEach(data['history'].slice(-30), function(hist_data) {
                        date = hist_data['Date'];
                        d = new Date(date);
                        d.setTime( d.getTime() + d.getTimezoneOffset()*60*1000 );
                        d = { x:  d,
                                 y: [ parseFloat( hist_data['Open'] ),
                                       parseFloat( hist_data['High'] ),
                                       parseFloat( hist_data['Low'] ),
                                       parseFloat( hist_data['Close'] )
                                     ] }
                        recent_history.push( d );
                    });
                    render_candlesticks(symbol, recent_history);
                })
                .error(function(data, status, headers) {
                    error = {'message': "Error API returned " + status + " for patterns for symbol " + symbol,
                             'priority': 'error' }
                    $(document).trigger("add-alerts", [error]);

                });
    }

    function render_candlesticks(symbol, history) {
            $scope.chart = new CanvasJS.Chart(symbol + "_chart",
                        {
                          backgroundColor: "#f5f5f5",
                          title:{ text: symbol, },
                          exportEnabled: false,
                          axisY: { includeZero:false, prefix: "$", title: "Prices", },
                          axisX: { interval:1, valueFormatString: "MMM-DD", },
                          toolTip: { content: "Date:{x}</br>" +
                                                      "<strong>Prices:</strong></br>" +
                                                      "Open:{y[0]}, Close:{y[3]}</br>" +
                                                      "High:{y[1]},Low:{y[2]}", },
                          data: [ { type: "candlestick", dataPoints: history } ]
                        });
            $scope.chart.render();
    };

    $scope.add_symbol = function(symbol) {
        console.log(symbol);
        console.log(  $scope.portfolio[symbol]   );
        if ( $scope.portfolio[symbol]  ) {
            console.log("Symbol already being fetched.");
            
        } else {
            console.log("No symbol. Retrieving data");
            if (symbol) {
                $http({
                        method  : 'GET',
                        url     : 'api/symbol/' + symbol,
                    })
                    .success(function(data) {
                        if ( data[symbol] ) {
                            data = data[symbol];
                            data['symbol'] = symbol;
                            $scope.portfolio[symbol] = data;
                            $('#' + symbol).show();
                            load_patterns(symbol);

                        } else {
                             error = {'message': "Error API returned no data for symbol " + symbol, 'priority': 'error' }
                             $(document).trigger("add-alerts", [error]);
                        }

                    })
                    .error(function(data, status, headers) {
                        error = {'message': "Error API returned " + status + " for symbol " + symbol,
                                 'priority': 'error' }
                        $(document).trigger("add-alerts", [error]);
                    });
             }
        }
    }

    $scope.add_search_symbol = function() {
        symbol = $('#symbolsearch').val().toUpperCase();
        $scope.add_symbol(symbol);
    }


});




$(function() {

    $("#symbolsearch")
        .focus()
        .autocomplete({
            source: function(request,response) {
                $.ajax({
                    beforeSend: function(){
                        $("span.help-inline").show();
                        $("span.label-info").empty().hide();
                    },
                    url: "http://dev.markitondemand.com/api/v2/Lookup/jsonp",
                    dataType: "jsonp",
                    data: {
                        input: request.term
                    },
                    success: function(data) {
                        response( $.map(data, function(item) {
                            return {
                                label: item.Symbol + ": " + item.Name + " (" +item.Exchange+ ")",
                                value: item.Symbol
                            }
                        }));
                        $("span.help-inline").hide();
                    }
                });
            },
            minLength: 0,
            select: function( event, ui ) {
                //console.log(ui.item);
                $("span.label-info").html("You selected " + ui.item.label).fadeIn("fast");
            },
            open: function() {
                //$(this).removeClass("ui-corner-all").addClass("ui-corner-top");
            },
            close: function() {
                //$(this).removeClass("ui-corner-top").addClass("ui-corner-all");
            }
        });




});