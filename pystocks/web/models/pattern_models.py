from sqlalchemy import Table, Column
from sqlalchemy import Boolean
from sqlalchemy import BigInteger, Integer
from sqlalchemy import Date, String, Text
from sqlalchemy import Float, Numeric

from . import Base


class Pattern_Accuracies(Base):
    __tablename__ = 'pattern_accuracies'

    symbol = Column(String(20), primary_key = True)
    date = Column(Date, primary_key = True)
    pattern = Column(String(220), primary_key = True)
    num_up = Column(Integer)
    num_down = Column(Integer)
    total = Column(Integer)
    num_reversals = Column(Integer)
