import urllib, time, os, re, csv
import requests

def fetchGF(googleticker):
    url="http://www.google.com/finance?&q="
    txt=urllib.urlopen(url+googleticker).read()
    k=re.search('id="ref_(.*?)">(.*?)<',txt)
    if k:
        tmp=k.group(2)
        q=tmp.replace(',','')
    else:
        q=None
    return q

print(time.ctime())
# Set local time zone to NYC
os.environ['TZ']='America/New_York'
time.tzset()
t=time.localtime() # string
print(time.ctime())

def combine(ticker):
    quote=fetchGF(ticker) # use the core-engine function
    t=time.localtime()    # grasp the moment of time
    output=[t.tm_year,t.tm_mon,t.tm_mday,t.tm_hour,  # build a list
            t.tm_min,t.tm_sec,ticker,quote]
    return output

def fetchYAHOO(ticker):
    url = "http://finance.yahoo.com/d/quotes.csv?s=msft&f=price"


quandl_api_key = '6NA1n_82zPYw5W_S4Rct'
def fetchQuandl(ticker):
    url = "https://www.quandl.com/api/v1/datasets/WIKI" \
          "/{ticker}.csv?auth_token={key}".format(key=quandl_api_key, ticker=ticker)
    data = requests.get(url)
    return data


tickers=["AAPL","GOOG","BIDU","IBM", \
         "INTC","MSFT","SPY"]

freq=600 # fetch data every 600 sec (10 min)

for ticker in tickers:
    print ticker
    data=fetchGF(ticker)
    print(data)
    #writer.writerow(data) # save data in the file
