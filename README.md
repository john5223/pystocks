# Ubuntu
sudo apt-get install gfortran libopenblas-dev liblapack-dev
sudo pip install -r requirements.txt --upgrade

# Arch
pacman -S lapack
sudo pip install -r requirements.txt --upgrade

