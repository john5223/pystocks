import requests, random, numpy


class GameOver(Exception):
    pass

class RotaAPI(object):

    def __init__(self):
        self.base_url = 'http://rota.praetorian.com/rota/service/play.php'
        self.cookies = {}

    def reset(self):
        resp = requests.get(self.base_url + "?request=new")
        self.cookies = dict(resp.cookies)
        board = self.board()
        board_array = numpy.array(list(board)).reshape((3,3))
        self.coordinates_lookup = {}
        self.coordinates = {}
        num = 0
        for y, row in enumerate(board_array):
            for x, data in enumerate(row):
                num += 1
                self.coordinates_lookup[num] = (x,y)
                self.coordinates[(x,y)] = num

    def status(self):
        return requests.get(self.base_url + '?request=status', cookies=self.cookies).json()

    def board(self):
        return self.status().get('data',{}).get('board')

    def free_spaces(self, board=None):
        board = board or self.board()
        return [i for i, x in enumerate(board, start=1) if x == '-']

    def my_spaces(self, board=None):
        board = board or self.board()
        return [i for i, x in enumerate(board, start=1) if x == 'p']

    def c_spaces(self, board=None):
        board = board or self.board()
        return [i for i, x in enumerate(board, start=1) if x == 'c']

    def place(self, x):
        return requests.get(self.base_url + '?request=place&location={x}'.format(x=x), cookies=self.cookies).json()

    def move(self, x, y):
        return requests.get(self.base_url + '?request=move&from={x}&to={y}'.format(x=x,y=y), cookies=self.cookies).json()

    def available_moves(self, board=None):
        board = board or self.board()
        my_spaces = self.my_spaces(board)
        my_coordinates = [self.coordinates_lookup[x] for x in my_spaces]

        free_spaces = self.free_spaces(board)
        free_coordinates = [self.coordinates_lookup[x] for x in free_spaces]

        available_moves = []
        for my_coord in my_coordinates:
            for coord in free_coordinates:
                dist = numpy.sqrt( (coord[0] - my_coord[0])**2 + (coord[1] - my_coord[1])**2 )
                #print "%s to %s  = %s " % (my_coord, coord, dist)
                if dist == 1:
                    x = self.coordinates[ my_coord ]
                    y = self.coordinates[ coord ]
                    available_moves.append( (x,y) )

        return available_moves


    def take_turn(self, board=None):
        board = board if board else self.board()
        if len(self.my_spaces(board)) < 3:
            resp = self.place_piece(board)
        else:
            resp = self.move_piece(board)
        return resp


    def place_piece(self, board=None):
        board = board or self.board()
        len_board = len(board)
        c_spaces = self.c_spaces(board)
        free_spaces = [x for x in self.free_spaces(board) ]
        print "Free spaces: %s" % free_spaces
        print "Oponent: %s" % c_spaces
        if not c_spaces:
            x = random.choice(free_spaces)
            x = 5
        else:
            opponent_move = c_spaces[0]
            x = (opponent_move + 3) % 9
            print x
            while x not in free_spaces:
                x += 1
                print "x: %s" % x

        resp = self.place(x)
        print "Placed %s  :  Response: %s" % (x,resp)
        board = resp.get('data',{}).get('board')
        return resp


    def move_piece(self, board=None):
        board = board or self.board()
        available_moves = self.available_moves(board)
        move = random.choice(available_moves)
        resp = self.move( move[0], move[1] )
        print "HERE!!"
        print "Moved %s  :  Response: %s" % (move,resp)
        return resp


    def __str__(self):
        ret = ''
        for r in numpy.array(list(self.board())).reshape((3,3)):
            row = []
            for c in r:
                row.append(c)
            ret += ' '.join(row) + '\n'
        return ret



game = RotaAPI()

while True:
    print "#" * 70
    print "Starting new game"
    print "#" * 70

    game.reset()
    status = game.status()
    computer_wins = status.get('data',{}).get('computer_wins')
    print game
    while computer_wins == 0:
        status = game.take_turn()
        computer_wins = status.get('data',{}).get('computer_wins')
        print status
        print game
        print
