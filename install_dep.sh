sudo apt-get install -y python-dev build-essential g++
sudo apt-get install -y libfreetype6-dev libxft-dev libpng12-dev
sudo apt-get install -y gfortran libopenblas-dev liblapack-dev libatlas-base-dev

sudo pip install -r requirements.txt --upgrade
